import * as dotenv from 'dotenv';
import 'reflect-metadata';

import { App } from '../src/api/app';
import DIContainer from '../src/inversify.config';
import { InstanceCleanupTask } from '../src/scheduled-task/InstanceCleanupTask.class';

require('source-map-support').install();

dotenv.config();

const app: App = DIContainer.resolve<App>(App);
app.start();

InstanceCleanupTask.start();


