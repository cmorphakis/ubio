
export enum ErrorType {
    SERVER = 'SERVER.ERROR',
    VALIDATION = 'VALIDATION.ERROR',
    NOT_FOUND = '404_ROUTE_NOT_FOUND',
}

export class ExpressError extends Error {

    errorType: ErrorType;
    errorFields: string[];

    constructor(message: string, type: ErrorType, field?: string) {
        super(message);
        this.errorType = type;
        if (field) {
            this.errorFields = [field];
        }
    }
}
