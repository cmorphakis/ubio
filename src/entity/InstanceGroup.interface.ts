import { IInstance } from './Instance.interface';

export interface IInstanceGroup {
    [groupName: string]: {[id: string]: IInstance}
}
