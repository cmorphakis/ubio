export interface IMetaData  {
    [key: string]: number | string | Date | null;
}

export interface IInstance {
    id: string;
    group: string;
    createdAt: number;
    updatedAt: number;
    meta?: IMetaData;
}
