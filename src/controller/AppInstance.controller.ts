import { IDataStore } from '../data-store';
import DIContainer, { TYPES } from '../inversify.config';
import { IInstance, IMetaData } from '../entity';
import {IReducer, IReducerResult} from '../reducer';

export class AppInstanceController {

    private static dataStore: IDataStore = DIContainer.get<IDataStore>(TYPES.IDataStore);
    private static reducer: IReducer = DIContainer.get<IReducer>(TYPES.IReducer);

    public static generateInstance(id: string, groupName: string, meta?: IMetaData): IInstance {
        const now = new Date().getTime();

        const instance: IInstance = {
            id,
            group: groupName,
            createdAt: now,
            updatedAt: now,
        };
        if (meta !== undefined) {
            instance.meta = meta;
        }

        return instance;
    }

    public static getGroupInstances(groupName: string): IInstance[] {
        return this.dataStore.getInstancesByGroupName(groupName);
    }

    public static insertOrUpdateInstance(instance: IInstance): IInstance {
        if (this.dataStore.instanceExists(instance)) {
            const updatedInstace = this.dataStore.updateInstance(instance);

            return updatedInstace;
        } else {
            this.dataStore.addInstance(instance);

            return instance;
        }
    }

    public static deleteInstance(instance: IInstance): void {
        if (this.dataStore.instanceExists(instance)) {
            this.dataStore.deleteInstance(instance);
        }
    }

    public static getInstancesSummary(): IReducerResult[] {
        return this.reducer.reduce(this.dataStore);
    }
}
