import {IInstance, IInstanceGroup} from '../entity';

export interface IDataStore {
    getInstancesByGroupName(groupName: string): IInstance[];
    getAllInstances(): IInstanceGroup;

    addInstance(instance: IInstance): void;
    deleteInstance(groupName: string, id: string): void;
    deleteInstance(instance: IInstance): void;
    updateInstance(instance: IInstance): IInstance;

    instanceExists(instance: IInstance): boolean
}





