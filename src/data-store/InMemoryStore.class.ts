import { injectable } from 'inversify';
import { IDataStore } from './DataStore.interface';
import * as winston from 'winston';
import { ExpressError, IInstance, IInstanceGroup } from '../entity';
import { ErrorType } from '../entity/ExpressError.class';

@injectable()
export class InMemoryStoreClass implements IDataStore {

    private store: IInstanceGroup;

    constructor() {
        this.store = {};
    }

    addInstance(instance: IInstance): void {
        const {group, id} = instance;
        try {
            if (this.store[group] !== undefined) {
                this.store[group][id] = instance;
            } else {
                this.store[group] = {[id]: instance};
            }
        } catch (error) {
            winston.error('InMemoryStore.addIstance', error);
            throw new ExpressError(error.message, ErrorType.SERVER, 'InMemoryStore.addIstance');
        }
    }

    getInstancesByGroupName(groupName: string): IInstance[] {
        if (this.store[groupName] !== undefined) {
            return Object.values(this.store[groupName]);
        }
        return [];
    }

    getAllInstances(): IInstanceGroup {
        return this.store;
    }

    deleteInstance(groupName: string, id: string): void;
    deleteInstance(instance: IInstance): void;
    deleteInstance(param1: IInstance | string, param2?: string): void {
        let group: string, id: string;

        if (typeof param1 === 'string' && typeof param2 === 'string') {
            group = param1;
            id = param2;
        } else {
            group = (param1 as IInstance).group;
            id = (param1 as IInstance).id;
        }
        try {
            delete this.store[group][id];
            // if group is empty, delete the group key as well
            if (Object.values(this.store[group]).length === 0) {
                delete this.store[group];
            }
        } catch (error) {
            winston.error('InMemoryStore.updateInstance', error);
            throw new ExpressError(error.message, ErrorType.SERVER, 'InMemoryStore.updateInstance');
        }
    }

    updateInstance(instance: IInstance): IInstance {
        const now = new Date().getTime();
        const { group, id } = instance;

        try {
            this.store[group][id].updatedAt = now;
            // TODO not sure whether metadata should be updated or not ??
            this.store[group][id].meta = instance.meta;
            return this.store[group][id];
        } catch (error) {
            winston.error('InMemoryStore.updateInstance', error);
            throw new ExpressError(error.message, ErrorType.SERVER, 'InMemoryStore.updateInstance');
        }
    }

    instanceExists(instance: IInstance): boolean {
        try {
            const { group, id } = instance;
            return this.store[group] && this.store[group][id] !== undefined;
        } catch (error) {
            winston.error('InMemoryStore.instanceExists', error);
            throw new ExpressError(error.message, ErrorType.SERVER, 'InMemoryStore.instanceExists');
        }
    };
}


