import {IDataStore} from '../data-store';
import DIContainer, {TYPES} from '../inversify.config';
import Timeout = NodeJS.Timeout;
import * as winston from "winston";
import { ExpressError } from '../entity';
import { ErrorType } from '../entity/ExpressError.class';

export class InstanceCleanupTask {

    private static dataStore: IDataStore = DIContainer.get<IDataStore>(TYPES.IDataStore);
    private static timeout: Timeout;

    public static start(taskInterval?: number, instanceTtl?: number): void {
        this.timeout = setInterval( () => {
            winston.info('Starting cleanup task...');
            try {
                this.removeExpiredInstances(instanceTtl);
            } catch (error) {
                winston.error('InMemoryStore.updateInstance', error);
                throw new ExpressError(error.message, ErrorType.SERVER, 'InstanceCleanupTask.removeExpiredInstances');
            }
        }, taskInterval || Number(process.env.CLEANUP_TASK_INTERVAL));
    }

    public static removeExpiredInstances(ttlMs?: number) {
        const instanceTtl = ttlMs || Number(process.env.INSTANCE_TTL);
        const appInstances = this.dataStore.getAllInstances();
        // the two for-loops below are blocking the event-loop which means that depending on the number of registered
        // instances, the service could become unresponsive to incoming requests while the loops are executing.
        // In a production scenario, this would need to be an async operation that uses a state lock (maybe Redis?)
        Object.entries(appInstances).forEach((groupKeyValue) => {
            const groupKey = groupKeyValue[0];
            const groupValue = groupKeyValue[1];

            Object.entries(groupValue).forEach((instanceKeyValue) => {
                const instanceKey = instanceKeyValue[0];
                const instanceValue = instanceKeyValue[1];

                if (new Date().getTime() - instanceValue.updatedAt >= instanceTtl) {
                    winston.info(`Deleting instance with id - ${instanceKey} - from group - ${groupKey}`);
                    this.dataStore.deleteInstance(groupKey, instanceKey);
                }
            });
        });
    }

    public static stop(): void {
        clearInterval(this.timeout);
    }
}
