import { injectable } from 'inversify';
import { IReducer, IReducerResult } from './Reducer.interface';
import { IDataStore } from '../data-store';

@injectable()
export class CountReducer implements IReducer {

    reduce(store: IDataStore): IReducerResult[] {

        const appInstances = store.getAllInstances();

        const appInstanceArray = Object.values(appInstances);
        const appInstanceArrayFlat = appInstanceArray.map(appInstance => Object.values(appInstance)).flat();

        const summary = appInstanceArrayFlat.reduce<{[key: string]: IReducerResult}>( (summary, instance) => {
            const group = instance.group;
            if (summary[group]) {
                summary[group].instances = ++summary[group].instances;
                summary[group].createdAt =  Math.min(summary[group].createdAt, instance.createdAt);
                summary[group].updatedAt =  Math.max(summary[group].updatedAt, instance.updatedAt);

            } else {
                summary[group] = {
                    instances: 1,
                    group: instance.group,
                    createdAt: instance.createdAt,
                    updatedAt: instance.updatedAt
                };
            }

            return summary;
        }, {});

        return Object.values(summary);
    }
}
