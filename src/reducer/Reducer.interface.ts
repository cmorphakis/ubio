import {IDataStore} from '../data-store';

export interface IReducerResult {
    group: string,
    instances: number,
    createdAt: number,
    updatedAt: number,
}

export interface IReducer {
    reduce(store: IDataStore): IReducerResult[];
}
