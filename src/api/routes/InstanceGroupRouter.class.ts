import { Application, Router } from 'express';
import { AppInstanceController } from '../../controller';
import { ExpressError, IMetaData } from '../../entity';
import { ErrorType } from '../../entity/ExpressError.class';


export class InstanceGroupRouter {

    public static configureRoutes(app: Application): void {

        const router = Router();

        router.route('/:group').get( (req, res, next) => {
            try {
                const { group } = req.params;
                const groupInstances = AppInstanceController.getGroupInstances(group);

                return res.send(groupInstances);
            } catch (error) {
                return next(error);
            }
        });

        router.route('/:group/:id').post( (req, res, next) => {
            try {
                const { group, id } = req.params;

                let meta: IMetaData | undefined = undefined;
                if (Object.keys(req.body).length > 0) {
                    meta = req.body;
                }

                let instance = AppInstanceController.generateInstance(id, group, meta);
                instance = AppInstanceController.insertOrUpdateInstance(instance);

                return res.send(instance);

            } catch (error) {
                return next(error);
            }
        });

        router.route('/:group/:id').delete( (req, res, next) => {
            try {
                const { group, id } = req.params;

                const instance = AppInstanceController.generateInstance(id, group);
                AppInstanceController.deleteInstance(instance);

                return res.status(204).send();
            } catch (error) {
                return next(error);
            }
        });

        router.route('/').get( (req, res, next) => {
                try {
                    const summary = AppInstanceController.getInstancesSummary();
                    return res.send(summary);
                } catch (error) {
                    return next(error);
                }
        });


        app.use('', router);
        app.use((req, res, next) => {
            const error = new ExpressError('Not Found', ErrorType.NOT_FOUND, req.url);
            next(error);
        });
    }
}
