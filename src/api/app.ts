import { config as dotenvConfig } from 'dotenv';
import { injectable } from 'inversify';
import { Application, NextFunction, Request, Response } from 'express';
import { Server } from 'http';
import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import * as http from 'http';

import { InstanceGroupRouter } from './routes/InstanceGroupRouter.class';
import { ExpressError } from '../entity';
import { ErrorType } from '../entity/ExpressError.class';
import * as winston from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';


dotenvConfig();

@injectable()
export class App {
    private readonly _app: Application;
    private server: Server;

    constructor() {
        this._app = express();
        this._app.use(bodyParser.json());
        this._app.use(bodyParser.urlencoded({extended: false}));
        this.setupLoggers();

        this.setupCors();
        this.setupRoutes();
        this.setupErrorHandling();

        this.server = http.createServer(this._app);
    }

    public start(): void {
        this._app.listen(process.env.EXPRESS_PORT, () => {
            winston.info('listening on port: ' + process.env.EXPRESS_PORT);
        });
    }

    public stop(): void {
        this.server.close();
    }

    public get app(): Application {
        return this._app;
    }

    public startPromise(): Promise<boolean> {
        return new Promise( (resolve, reject) => {
            this._app.listen(process.env.EXPRESS_PORT, () => {
                winston.info('listening on port: ' + process.env.EXPRESS_PORT);
                resolve(true);
            });
        });
    }

    private setupRoutes(): void {
        InstanceGroupRouter.configureRoutes(this._app);
    }

    private setupErrorHandling(): void {
        this._app.use((error: ExpressError, req: Request, res: Response, next: NextFunction) => {
            if (error.errorType === ErrorType.VALIDATION) {
                res.status(400).send({error: error.message, fields: error.errorFields});
            } else if (error.errorType === ErrorType.NOT_FOUND) {
                res.status(404).send({error: error.message, fields: error.errorFields});
            } else {
                res.status(500).send({error: 'Internal Server Error'});
            }
        });
    }

    private setupCors(): void {
        // ** NOTE ** This is too permissive and thus a security vulnerability as the current CORS configuration
        // allows requests from all origins.
        this._app.use(cors());
    }

    public setupLoggers(): this {
        const winstonMorganLogger = winston.createLogger();

        winstonMorganLogger.add(
            new DailyRotateFile({
                filename: 'logs/http-requests-log-%DATE%.log',
                datePattern: 'YYYY-MM-DD',
                json: false
            })
        );

        winston.add(
            new DailyRotateFile({
                format: winston.format.combine(
                    winston.format.timestamp(),
                    winston.format.json()),
                filename: 'logs/service-log-%DATE%.log',
                datePattern: 'YYYY-MM-DD',
                json: false
            })
        );

        winston.add(new winston.transports.Console({ format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.simple()
            ) }));

        const morganStream = {
            write: (text: string) => {
                winstonMorganLogger.info(text.trim()); // proxy everything to winston
            }
        };

        this._app.use(morgan('combined', { stream: morganStream }));
        this._app.use(morgan('short'));

        winston.info('the loggers have been set successfully...');
        return this;
    }
}
