export class ServerUtils {
    public static sleep(milliSeconds: number): Promise<void> {
        return new Promise((resolve, reject) => {
            setTimeout( () => {
                resolve();
            }, milliSeconds);
        });
    }
}
