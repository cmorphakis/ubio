import { Container } from 'inversify';
import { IDataStore, InMemoryStoreClass } from './data-store';
import { IReducer } from './reducer';
import { CountReducer } from './reducer/CountReducer.class';
import {App} from "./api/app";

// The inversify module is used to inject dependencies at runtime as defined in this config file.
// Any class that is 'likely' to change has an abstract interface which is implemented by all derived concrete classes.
// This achieves a more decoupled architecture where components do not rely on other concrete components.
export const TYPES = {
    IDataStore: Symbol.for('IDataStore'),
    IReducer: Symbol.for('IReducer'),
    App: Symbol.for('App'),
};

const DIContainer = new Container();
DIContainer.bind<IDataStore>(TYPES.IDataStore).to(InMemoryStoreClass).inSingletonScope();
DIContainer.bind<IReducer>(TYPES.IReducer).to(CountReducer).inSingletonScope();
DIContainer.bind<App>(TYPES.App).to(App).inSingletonScope();

export default DIContainer;

