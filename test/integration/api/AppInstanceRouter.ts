import 'mocha';
import 'reflect-metadata';

import * as assert from 'assert';
import * as request from 'supertest';
import { v4 as uuid } from 'uuid';

import { AppInstanceController } from "../../../src/controller";
import { ServerUtils } from "../../../src/util/ServerUtils";
import { IReducerResult } from "../../../src/reducer";
import { InstanceCleanupTask } from "../../../src/scheduled-task/InstanceCleanupTask.class";
import { IInstance } from "../../../src/entity";
import { App } from "../../../src/api/app";

declare let global: {
    app: App
};

describe('AppInstanceRouter tests', function ()  {
    this.timeout(60 * 2 * 1000);

    const group1 = 'TEST.GROUP.ROUTER.1';
    const id1 = 'TEST.ROUTER.ID.1';

    it('GET / - returns a summary of all currently registered groups',async () => {

        const summaryTestGroup1 = 'TEST.SUMMARY.GROUP.1';
        const summaryTestGroup2 = 'TEST.SUMMARY.GROUP.2';
        InstanceCleanupTask.start(2000, 10000);

        const instance1 = AppInstanceController.generateInstance(uuid(), summaryTestGroup1);
        AppInstanceController.insertOrUpdateInstance(instance1);

        await ServerUtils.sleep(8000);

        // add a second instance to TEST.SUMMARY.GROUP.1:
        const instance3 = AppInstanceController.generateInstance(uuid(), summaryTestGroup1);
        AppInstanceController.insertOrUpdateInstance(instance3);

        await ServerUtils.sleep(2000);
        // update the instance1
        const updatedInstance1 = AppInstanceController.insertOrUpdateInstance(instance1);
        AppInstanceController.insertOrUpdateInstance(updatedInstance1);
        // add a new instance to TEST.SUMMARY.GROUP.2:
        const instance2 = AppInstanceController.generateInstance(uuid(), summaryTestGroup2);
        AppInstanceController.insertOrUpdateInstance(instance2);

        await (() => {
            return new Promise( (resolve) => {
                request(global.app.app)
                    .get(`/`)
                    .expect(200)
                    .end((err, res) => {
                        // currently there should be 2 groups
                        assert.strictEqual(res.body.length, 2);
                        const group1 = res.body.filter((summary: IReducerResult) => summary.group === summaryTestGroup1)[0] as IReducerResult;
                        const group2 = res.body.filter((summary: IReducerResult) => summary.group === summaryTestGroup2)[0] as IReducerResult;

                        // group1 should have 2 instances
                        assert.strictEqual(group1.instances, 2);
                        // group2 should have 1 instance
                        assert.strictEqual(group2.instances, 1);

                        // group1 should have the createdAt timestamp of the first instance
                        assert.strictEqual(group1.createdAt, instance1.createdAt);
                        // group1 should have the updatedAt timestamp of the last updated or inserted instance
                        assert.strictEqual(group1.updatedAt, updatedInstance1.updatedAt);
                        // the createdAt of the updatedInstance and instance1 should be equal
                        assert.strictEqual(group1.createdAt, updatedInstance1.createdAt);
                        resolve();
                    });
            });
        })();

        await ServerUtils.sleep(8000);
        await (() => {
            return new Promise( (resolve) => {
                request(global.app.app)
                    .get(`/`)
                    .expect(200)
                    .end((err, res) => {
                        assert.strictEqual(res.body.length, 2);
                        const group1 = res.body.filter((summary: IReducerResult) => summary.group === summaryTestGroup1)[0] as IReducerResult;
                        const group2 = res.body.filter((summary: IReducerResult) => summary.group === summaryTestGroup2)[0] as IReducerResult;

                        // group1 should now have 1 instance since one of them should have expired
                        assert.strictEqual(group1.instances, 1);
                        // group2 should have 1 instance
                        assert.strictEqual(group2.instances, 1);
                        assert.strictEqual(group1.createdAt, updatedInstance1.createdAt);

                        assert.strictEqual(group2.createdAt, instance2.createdAt);
                        assert.strictEqual(group2.updatedAt, instance2.updatedAt);
                        // since instance2 has never been updated, their update and creation timestamps should be equal
                        assert.strictEqual(instance2.createdAt, instance2.updatedAt);
                        resolve();
                    });
            });
        })();
        InstanceCleanupTask.stop();
    });

    it('POST /:group/:id - Add a new instance' , () => {
        request(global.app.app)
            .post(`/${group1}/${id1}`)
            .expect(200)
            .end((err, res) => {
                assert.strictEqual(res.body.id, id1);
                assert.strictEqual(res.body.group, group1);
                assert.strictEqual(res.body.createdAt, res.body.updatedAt);

        });
    });

    it('POST /:group/:id - Update an existing instance' , () => {
        request(global.app.app)
            .post(`/${group1}/${id1}`)
            .expect(200)
            .end((err, res) => {
                assert.strictEqual(res.body.id, id1);
                assert.strictEqual(res.body.group, group1);
                assert.strictEqual(res.body.createdAt < res.body.updatedAt, true);
            });
    });

    it('POST /:group/:id - Add an instance with metadata' , () => {
        const metaData = {key1: 'val1', key2: 'val2', nestedKey: {key3: 'val3'}};

        request(global.app.app)
            .post(`/${group1}/${id1}`)
            .send(metaData)
            .expect(200)
            .end((err, res) => {
                assert.strictEqual(res.body.id, id1);
                assert.strictEqual(res.body.group, group1);
                assert.strictEqual(res.body.createdAt < res.body.updatedAt, true);
                assert.deepStrictEqual(res.body.meta, metaData);
            });
    });

    it('DELETE /:group/:id - removes an instance' , () => {
        const tmpGroup = 'TEST.DELETE.GROUP';
        const tmpId = 'TEST.DELETE.ID';

        const instance = AppInstanceController.generateInstance(tmpId, tmpGroup);
        AppInstanceController.insertOrUpdateInstance(instance);

        const groupInstancesBeforeDeletion = AppInstanceController.getGroupInstances(tmpGroup);
        assert.strictEqual(groupInstancesBeforeDeletion.length, 1);

        request(global.app.app)
            .delete(`/${tmpGroup}/${tmpId}`)
            .expect(204)
            .end((err, res) => {
                assert.deepStrictEqual(res.body, {});
                const groupInstancesAfterDeletion = AppInstanceController.getGroupInstances(tmpGroup);
                assert.strictEqual(groupInstancesAfterDeletion.length, 0);
            });
    });


    it('GET /:group - returns a list of all registered instances within the group',async () => {
        const tmpGroup1 = 'TEST.GET.GROUP.1';
        const tmpGroup2 = 'TEST.GET.GROUP.2';

        const uuid1 = uuid();
        const uuid2 = uuid();
        const uuid3 = uuid();
        // add 1 instance to tmpGroup1:
        const instance = AppInstanceController.generateInstance(uuid1, tmpGroup1);
        AppInstanceController.insertOrUpdateInstance(instance);

        // add 2 instances to tmpGroup2:
        const instance2 = AppInstanceController.generateInstance(uuid2, tmpGroup2);
        const instance3 = AppInstanceController.generateInstance(uuid3, tmpGroup2);
        AppInstanceController.insertOrUpdateInstance(instance2);
        AppInstanceController.insertOrUpdateInstance(instance3);

        request(global.app.app)
            .get(`/${tmpGroup1}`)
            .expect(200)
            .end((err, res) => {
                assert.deepStrictEqual(res.body.length, 1);
                const returnedInstance = res.body[0] as IInstance;
                assert.deepStrictEqual(returnedInstance, instance);
        });

        request(global.app.app)
            .get(`/${tmpGroup2}`)
            .expect(200)
            .end((err, res) => {
                assert.deepStrictEqual(res.body.length, 2);
                const returnedInstanceUuid2 = res.body.filter((instance: IInstance) => instance.id === uuid2)[0] as IInstance;
                const returnedInstanceUuid3 = res.body.filter((instance: IInstance) => instance.id === uuid3)[0] as IInstance;

                assert.deepStrictEqual(returnedInstanceUuid3, instance3);
                assert.deepStrictEqual(returnedInstanceUuid2, instance2);
        });
    });
});
