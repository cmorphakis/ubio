import 'mocha';
import * as assert from 'assert';

import { IInstance } from "../../../src/entity";
import { AppInstanceController } from "../../../src/controller";

describe('AppInstanceController tests', () => {

    describe('The AppInstanceController methods should return the correct results', () => {

        let instance: IInstance;
        const instanceGroup = 'TEST.GROUP.1';
        const instanceGroup2 = 'TEST.GROUP.2';
        const instanceId = 'TEST.ID.1';
        const instanceId2 = 'TEST.ID.2';

        let instanceWithMeta: IInstance;
        const meta = {
            metadata: 'test-meta'
        };

        before( () => {
            instance = AppInstanceController.generateInstance(instanceId, instanceGroup);
            instanceWithMeta = AppInstanceController.generateInstance(instanceId, instanceGroup2, meta);
        });

        it('Generated app instances should have the correct properties', () => {
            assert.strictEqual(instance.group, instanceGroup);
            assert.strictEqual(instance.id, instanceId);
            assert.strictEqual(instance.createdAt, instance.updatedAt);
            assert.strictEqual(instance.meta, undefined);

            assert.strictEqual(instanceWithMeta.group, instanceGroup2);
            assert.strictEqual(instanceWithMeta.id, instanceId);
            assert.strictEqual(instanceWithMeta.createdAt, instanceWithMeta.updatedAt);
            assert.deepStrictEqual(instanceWithMeta.meta, meta);
        });

        it ('A new instance should be added to the data-store if it does not exist already', () => {
            let groupInstances = AppInstanceController.getGroupInstances(instanceGroup);
            assert.strictEqual(groupInstances.length, 0);

            AppInstanceController.insertOrUpdateInstance(instance);
            groupInstances = AppInstanceController.getGroupInstances(instanceGroup);

            assert.strictEqual(groupInstances.length, 1);
            // On creation, the createdAt and updatedAt timestamps should be equal
            assert.strictEqual(groupInstances[0].createdAt, groupInstances[0].updatedAt);
        });

        it ('A existing instance should not be added to the data-store if it exists already', () => {
            AppInstanceController.insertOrUpdateInstance(instance);
            const groupInstances = AppInstanceController.getGroupInstances(instanceGroup);
            assert.strictEqual(groupInstances.length, 1);
            // On update, the createdAt should < updatedAt
            assert.strictEqual(groupInstances[0].createdAt < groupInstances[0].updatedAt, true);
        });

        it ('Instances should be added under the correct group', () => {
            AppInstanceController.insertOrUpdateInstance(instanceWithMeta);

            const groupInstances = AppInstanceController.getGroupInstances(instanceGroup);
            assert.strictEqual(groupInstances.length, 1);
            assert.strictEqual(groupInstances[0].createdAt < groupInstances[0].updatedAt, true);

            const group2Instances = AppInstanceController.getGroupInstances(instanceGroup2);
            assert.strictEqual(group2Instances.length, 1);
            assert.strictEqual(group2Instances[0].createdAt, group2Instances[0].updatedAt);
        });

        it ('App instance should be produce the correct instance summary', () => {
            //generate a new instance for TEST.GROUP.1:
            const newInstance = AppInstanceController.generateInstance(instanceId2, instanceGroup);
            AppInstanceController.insertOrUpdateInstance(newInstance);

            const summary = AppInstanceController.getInstancesSummary();
            const groupSummary1 = summary.filter((groupSummary) => groupSummary.group === instanceGroup)[0];

            assert.strictEqual(groupSummary1.group, instanceGroup);
            assert.strictEqual(groupSummary1.instances, 2);
            assert.strictEqual(groupSummary1.createdAt, instance.createdAt);
            assert.strictEqual(groupSummary1.updatedAt, newInstance.updatedAt);
        });

        it ('After deletion, the correct instance should be removed from the group', () => {
            const groupInstancesBeforeDeletion = AppInstanceController.getGroupInstances(instanceGroup);
            AppInstanceController.deleteInstance(instance);
            const groupInstancesAfterDeletion = AppInstanceController.getGroupInstances(instanceGroup);
            assert.strictEqual(groupInstancesAfterDeletion.length, groupInstancesBeforeDeletion.length - 1);

            // the other groups should remain intact
            const group2Instances = AppInstanceController.getGroupInstances(instanceGroup2);
            assert.strictEqual(group2Instances.length, 1);
        });
    });
});
