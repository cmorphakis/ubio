import * as dotenv from 'dotenv';
import 'reflect-metadata';
import 'mocha';
import * as assert from 'assert';
import * as winston from "winston";

import { App } from '../src/api/app';
import DIContainer from '../src/inversify.config';

let serverRunning: boolean;
dotenv.config();
declare let global: {
    app: App
};

global.app = DIContainer.resolve<App>(App);
before( async () => {

    winston.info('running before global hook...');
    serverRunning = await global.app.startPromise();
    require('source-map-support').install();
    assert.strictEqual(serverRunning, true);
});

after( () => {
    global.app.stop();
   process.exit(0);
});


