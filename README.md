# ub.io coding assignment

## General
- To run locally, you will need to have Node.js installed on your system ( >= v.12 ) as the code uses some 
es2019 features.
- You will also need to have typescript installed. You can install it through npm: `npm install -g typescript`

## Installation
- Run `npm install` from the root directory of the project

## Start the Service
- Run `npm start`

## Run the tests
- Run `npm test`
